import multiprocessing
from multiprocessing import cpu_count, Pool
import requests
from bs4 import BeautifulSoup
from utils import utils
import warnings
import html
from config import config
import time
import re
import urllib
from fake_useragent import UserAgent

warnings.filterwarnings('ignore')


def google_results(keyword, n_results):
    # Getting top links from google using BeautifulSoup
    try:
        query = keyword
        query = urllib.parse.quote_plus(query)
        number_result = n_results
        ua = UserAgent()
        google_url = "https://www.google.com/search?q=" + query + "&num=" + str(number_result)
        response = requests.get(google_url, {"User-Agent": ua.random})
        if response.status_code == 429:
            print(response.status_code)
            pass
        soup = BeautifulSoup(response.text, "html.parser")
        result = soup.find_all('div', attrs={'class': 'ZINbbc'})
        results = [re.search('\/url\?q\=(.*)\&sa', str(i.find('a', href=True)['href'])) for i in result if "url" in str(i)]
        links = [i.group(1) for i in results if i is not None]
        return links
    except Exception as e:
        print(e)
        pass


def getProductsFromLinks(link, product, rank, similarProduct, MBProducts):
    try:
        response = requests.get(link)
        if response.status_code == 429:
            time.sleep(5)
            pass
        productHtmlText = response.content
        productPage = BeautifulSoup(productHtmlText, features="html.parser")
        [p.extract() for p in productPage(['style', 'script', '[document]', 'head', 'title'])]
        requiredProductPageText = str(productPage).lower()
        extractedProductDict = dict()
        parentProductDict = MBProducts[product]
        parentProductModel = parentProductDict["features"]["brand"]
        parentProductSpecifications = parentProductDict["features"]["specifications"]
        textWithoutHtmlTags = utils.removeHtmlTags(requiredProductPageText)
        # extractedProductDict["rank"] = str(rank)
        extractedProductDict["description"] = utils.extractDescription(requiredProductPageText)
        extractedProductDict["review"] = utils.extractReview(requiredProductPageText)
        extractedProductDict["price"] = utils.getPriceFromText(textWithoutHtmlTags)
        extractedProductDict["listItems"] = utils.extractListItemsFromHtml(requiredProductPageText)
        tables = utils.getAllTableFromHtml(requiredProductPageText)
        headings = utils.extractHeadingFromHtml(requiredProductPageText)
        extractedProductDict["features"] = utils.createGeneralDictForFeatures(tables + headings)
        extractedProductDict["productNames"] = utils.extractProductNameFromUrl(link)
        desc = utils.removeJunkFromDescriptions(extractedProductDict["description"]).replace('"', '')
        desc = ''.join([i if ord(i) < 128 else ' ' for i in desc])
        features = str(extractedProductDict["features"])
        features = ''.join([i if ord(i) < 128 else ' ' for i in features])

        if utils.isProductRelevant(extractedProductDict, parentProductDict, requiredProductPageText,
                                   featureMatchingRequired=True):
            print("Relevant Product -> ", link)
            moreCheckForRelevancy = utils.checkProductFeaturesRelevancy(extractedProductDict, parentProductDict)
            # imgs = utils.getImageUrls(link, parentProductModel)
            # print("imgs urls = ", imgs)
            # for img in imgs:
            # print("@@@->", img)
            #    utils.extractImagesFromUrl(img, product)
            # print(str(product) + "*" + str(link) + "*" + str(rank) + "*" + str(json.dumps(extractedProductDict["price"])) + "*" + json.dumps(extractedProductDict["review"]) + "*" + json.dumps(extractedProductDict["features"]) + "*" + json.dumps(utils.removeJunkFromDescriptions(extractedProductDict["description"])) + "$")
            with open("output/excelOutput.txt", 'a', 1) as f:
                f.write(str(product) + "``" + str(link) + "``" + str(rank) + "``" +
                        str(extractedProductDict["price"]).replace('"', '') + "``" +
                        str(extractedProductDict["review"]).replace('"', '') + "``" +
                        str(features) + "``" +
                        str(desc) + "``" +
                        str(moreCheckForRelevancy) + "``" +
                        str(parentProductSpecifications) + "``" +
                        str(parentProductDict["description"]) + "``" +
                        str(parentProductDict["price"]) + "``" +
                        str(parentProductDict["category"]) + "``" +
                        str(parentProductDict["entityId"]) + "~~")

            similarProduct[link] = extractedProductDict
    except Exception as e:
        print(e)
        pass


if __name__ == "__main__":
    try:
        # converting input excel file to product objects
        MBProducts = utils.extractProductFromExcel(config.inputFilePath)
        # getting the product names from the MBProducts dict/object
        MBProductsList = list(MBProducts.keys())
        # creating multiprocessing queues
        queue = multiprocessing.Queue()
        processes = []
        pool = Pool(processes=10)
        total_pool_count = int(int(cpu_count()) / 2.0)
        try:
            for product in MBProductsList:
                print("Product Name =", product)
                similarProduct = dict()
                # linkList = utils.extractSimilarProductsLinksFromGoogleForSingleProduct(product)
                linkList = google_results(product, config.numOfUrlsFromGoogle)
                # print(linkList)
                rank = 1
                try:
                    for link in linkList:
                        if not any(x in link for x in config.stringNotToBePresentInLink):
                            print("Link -> " + link)
                            pool.apply_async(getProductsFromLinks, args=(link, product, rank, similarProduct, MBProducts))
                            rank += 1
                except Exception as e:
                    print(e)
                    pass
        except Exception as e:
            print(e)
            pass

        pool.close()
        pool.join()
    except Exception as e:
        print(e)
        pass
