import xlsxwriter
import os
import html
from config import config
from datetime import datetime

currTime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

workbook = xlsxwriter.Workbook(config.outputFilePath + currTime + ".xlsx")
worksheet = workbook.add_worksheet()
inputFile = open("output/excelOutput.txt", "r")
inputFile = inputFile.read()
inputFile = html.unescape(inputFile)
inputFile = inputFile[0: -1].split("~~")

column = 0
row = 0
colHeading = ['Product Name', 'Link', 'Rank', 'Price', 'Review', 'Features', 'Description']

for header in colHeading:
    worksheet.write(row, column, header)
    column += 1
row = 1
for line in inputFile:
    prdDetails = line.split("``")
    if len(prdDetails) > 6:
        worksheet.write(row, 0, prdDetails[0])
        worksheet.write(row, 1, prdDetails[1])
        worksheet.write(row, 2, prdDetails[2])
        worksheet.write(row, 3, prdDetails[3])
        worksheet.write(row, 4, prdDetails[4])
        worksheet.write(row, 5, prdDetails[5])
        worksheet.write(row, 6, prdDetails[6].replace('"', ''))
        row += 1

workbook.close()

os.remove("output/excelOutput.txt")

