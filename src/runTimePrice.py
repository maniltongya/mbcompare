import pandas as pd
import requests
from flask import Flask, request
from bs4 import BeautifulSoup
from utils import utils
from config import config

app = Flask(__name__)

relevantProducts = pd.read_excel("reports/ProductReport.xlsx")
relevantProducts['Price'] = relevantProducts['Price'].fillna(0)


def getPriceFromRelevantProducts(product):
    if product in relevantProducts.values:
        productDF = relevantProducts.loc[relevantProducts['Product Name'] == product]
        updatedPriceObject = dict()
        listOfRelevantProducts = []
        for i in range(len(productDF)):
            link = productDF.iloc[i]['Link']
            print(link)
            if not any(x in link for x in config.stringNotToBePresentInLink):
                oldPrice = productDF.iloc[i]['Price']
                newPrice = 0.0
                if oldPrice > 0:
                    response = requests.get(link)
                    if response.status_code == 429:
                        pass
                    productHtmlText = response.content
                    productPage = BeautifulSoup(productHtmlText, features="html.parser")
                    [p.extract() for p in productPage(['style', 'script', '[document]', 'head', 'title'])]
                    requiredProductPageText = str(productPage).lower()
                    textWithoutHtmlTags = utils.removeHtmlTags(requiredProductPageText)
                    newPrice = utils.getPriceFromText(textWithoutHtmlTags)
                listOfRelevantProducts.append([link, oldPrice, newPrice])
            updatedPriceObject[product] = listOfRelevantProducts
        return updatedPriceObject
    else:
        return "Product is not present"


@app.route('/', methods=["POST"])
def getRunTimePrice():
    if request.method == "POST":
        product = request.values['productName']
        if product:
            print("Fetching RunTime Price For Product - "+str(product))
            return getPriceFromRelevantProducts(product)
    return False
