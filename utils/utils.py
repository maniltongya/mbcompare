import requests
import urllib.request
from googlesearch import search
from bs4 import BeautifulSoup
import re
from collections import OrderedDict
import pandas as pd
import xlrd
from price_parser import Price
from config import config
import spacy
import warnings
import os
from urllib.parse import urljoin, urlparse
import urllib.request, urllib.error
from PIL import Image
import html
import difflib
import time

warnings.filterwarnings('ignore')


def extractProductInfo(productDict):
    # Extracting product information
    productInfo = dict()
    for product in productDict:
        productDetails = dict()
        rank = 1
        for link in productDict[product]:
            # checking of below words should not be present in the links as it would cost the time out for html data extraction
            # and also most of them are ecommerce weblinks these are mostly blogs
            if config.stringNotToBePresentInLink not in link:
                productData = dict()
                # print("Link -> " + link)
                try:
                    response = requests.get(link)
                    productHtmlText = response.content
                    productPage = BeautifulSoup(productHtmlText, features="html.parser")
                    [p.extract() for p in productPage(['style', 'script', '[document]', 'head', 'title'])]
                    productData["rank"] = rank
                    productData["data"] = str(productPage)
                    requiredProductPageText = productData
                    rank += 1

                    productDetails[link] = requiredProductPageText
                except:
                    pass
        productInfo[product] = productDetails
    return productInfo


def removeHtmlTags(htmlText):
    cleanTags = re.compile('<.*?>')
    textAfterRemovingTags = re.sub(cleanTags, ' ', htmlText)
    return textAfterRemovingTags


def extractSimilarProductsLinksFromGoogle(productList):
    productDict = dict()
    for product in productList:
        try:
            productDict[product] = search(product, tld='co.in', lang='en', num=config.numOfUrlsFromGoogle, start=0,
                                          stop=config.numOfUrlsFromGoogle, pause=2.0)
        except:
            pass
    return productDict


def extractSimilarProductsLinksFromGoogleForSingleProduct(product):
    productDict = []
    try:
        for j in search(product, tld='com', lang='en', num=config.numOfUrlsFromGoogle, start=0,
                        stop=config.numOfUrlsFromGoogle, pause=2.0):
            productDict.append(j)
    except Exception as e:
        print(e)
        pass
    return productDict


def extractProductFromExcel(loc):
    # extracting products from excel input file
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    sheet.cell_value(0, 0)
    MBProductDict = dict()

    for i in range(1, sheet.nrows):
        tempDict = dict()
        productName = sheet.cell_value(i, 0).replace("\xa0", " ")
        for j in range(sheet.ncols):
            featuresDict = dict()
            featuresDict["brand"] = sheet.cell_value(i, 1).lower()
            featuresDict["model"] = sheet.cell_value(i, 2).lower()
            featuresDict["specifications"] = sheet.cell_value(i, 4).lower()
            tempDict["features"] = featuresDict
            tempDict["description"] = sheet.cell_value(i, 3).lower()
            tempDict["category"] = sheet.cell_value(i, 5).lower()
            tempDict["price"] = str(sheet.cell_value(i, 6))
            tempDict["entityId"] = str(sheet.cell_value(i, 7))
        MBProductDict[productName] = tempDict

    return MBProductDict


def extractDescription(text):
    # extracting description from html page data
    description = []
    text = text.lower()
    # descriptionRelatedText = ['description', 'feature', 'specification', 'detail']
    descriptionRelatedText = ['description', 'specification', 'detail', 'information']
    for word in descriptionRelatedText:
        if word in text:
            descriptionText = text.split(word)[1]
            des = re.findall(r'>(.*?)<', descriptionText)
            # extraction the description data for next five lines only
            des = [d for d in des if len(d) > 5][:5]
            description += des
    description = list(OrderedDict.fromkeys(description))
    for des in description:
        des = " ".join(re.sub('[^A-Za-z0-9 ]+', '', des).split())
    return description


def removeJunkFromDescriptions(listOfDescription):
    description = " ".join(listOfDescription)
    removeWords = config.removeWordsFromDescription
    for word in removeWords:
        description = description.replace(word, "")
    return html.unescape(description)


def extractReview(text):
    review = []
    text = text.lower()
    reviewRelatedText = ['review', 'rating']
    for word in reviewRelatedText:
        if word in text:
            reviewText = text.split(word)[1]
            # for revText in reviewText:
            rev = re.findall(r'>(.*?)<', reviewText)
            rev = [r for r in rev if len(r) > 1 and re.search('\d', r)][:2]
            review += rev
    review = list(OrderedDict.fromkeys(review))
    review = ' '.join(review)
    review = re.findall('\d*\.\d+', review)
    review = list(OrderedDict.fromkeys(review))
    review = [r for r in review if 5 >= float(r) >= 1]
    for rev in review:
        rev = " ".join(re.sub('[^A-Za-z0-9 ]+', '', rev).split())
    if len(review) > 0:
        return review[0]
    return ""


def extractPrice(text):
    text = text.lower()
    prices = []
    new = 0.0
    old = -1.0
    while new != old and new is not None and old is not None:
        try:
            old = new
            new = Price.fromstring(text).amount
            if new is not None and old is not None:
                prices.append(new)
            text = text.replace(str(new), "")
        except:
            pass
    # prices = list(map(str, prices))
    lenPrices = len(prices)
    if 'not available' in text or 'unavailable' in text or 'ask for price' in text or (
            'request a quote' in text and lenPrices == 0):
        return ""
    price = ""
    # print(prices)
    if lenPrices > 0:
        if lenPrices > 1:
            if lenPrices == 2:
                val1 = float(prices[0])
                val2 = float(prices[1])
                price = str(max(val1, val2))

            val = float(prices[0])
            if val < 1:
                price = str(prices[1])
            else:
                price = str(prices[0])
        if lenPrices == 1 or lenPrices > 2:
            price = str(prices[0])
    return price


def getPriceFromText(text):
    text = text.lower()
    prices = re.findall(r'(?:rs\.?|inr|₹)\s*(\d+(?:[.,]\d+)*)|(\d+(?:[.,]\d+)*)\s*(?:rs\.?|inr|₹)', text)
    price = 0.0
    allPrices = []
    for p in prices:
        if p[0] != '' and float(p[0].replace(",", "")) > 10:
            allPrices.append(float(p[0].replace(",", "")))
    lenPrices = len(allPrices)
    # print("Prices =", allPrices)
    if lenPrices > 0:
        price = allPrices[0]
    # If it is a B2B website and the price of the product is not available then in that case price should be empty
    if price == "0.0" or price == 0.0 or 'not available' in text or 'ask for quote' in text or 'get price' in text or 'request for price' in text or 'unavailable' in text or 'ask for price' in text or 'ask latest price' in text or 'request a quote' in text or len(
            prices) == 0:
        return ""
    if price > 4000:
        return ""
    return price


def extractListItemsFromHtml(htmlText):
    htmlListData = []
    data = BeautifulSoup(htmlText, 'html')
    data1 = data.find('ul')
    if data1:
        for li in data1.find_all("li"):
            htmlListData.append(li.text)
    return htmlListData


def getAllTableFromHtml(htmlText):
    # extracting the information from html table
    df_table = []
    finalTable = []
    try:
        df_table = pd.read_html(htmlText)
    except:
        pass
    for tab in df_table:
        tempTable = list(tab.itertuples(index=False, name=None))
        finalTable += tempTable
    return finalTable


def getCosineSimilarityForText(internalText, externalText):
    internal_list = word_tokenize(internalText)
    external_list = word_tokenize(externalText)

    sw = stopwords.words('english')
    l1 = []
    l2 = []

    internal_set = {w for w in internal_list if not w in sw}
    external_set = {w for w in external_list if not w in sw}
    rvector = internal_set.union(external_set)
    for w in rvector:
        if w in internal_set:
            l1.append(1)  # create a vector
        else:
            l1.append(0)
        if w in external_set:
            l2.append(1)
        else:
            l2.append(0)

    c = 0
    for i in range(len(rvector)):
        c += l1[i] * l2[i]
    cosine = c / float((sum(l1) * sum(l2)) ** 0.5)
    return cosine


def getTFIDFScoreForSentences(internalText, externalText):
    nlp = spacy.load('en_core_web_sm')
    internalTextNlp = nlp(internalText)
    externalTextNlp = nlp(externalText)
    sim1 = internalTextNlp.similarity(externalTextNlp)
    sim2 = externalTextNlp.similarity(internalTextNlp)
    return max(sim1, sim2)


def extractHeadingFromHtml(htmlText):
    htmlHeadingData = []
    data = BeautifulSoup(htmlText, 'html')
    headings = ['h1', 'h2', 'h3']
    for heading in headings:
        data1 = data.find_all(heading)
        if data1:
            for val in data1:
                htmlHeadingData.append(val.text)
    data2 = data.find('title')
    if data2:
        htmlHeadingData.append(data2.val)
    finalHeadingData = []
    for val in htmlHeadingData:
        val = " ".join(re.sub('[^A-Za-z0-9 ]+', '', val).split())
        finalHeadingData.append(val)
    return finalHeadingData


def extractProductNameFromUrl(productUrl):
    productName = str(productUrl).split('/')
    finalProductNames = []
    for product in productName:
        text = " ".join(re.sub('[^A-Za-z0-9 ]+', ' ', product).split())
        if text != '' and 'https' not in text:
            finalProductNames.append(text)
    return finalProductNames


def isProductRelevant(externalProduct, internalProduct, externalHtmlData, featureMatchingRequired):
    if externalProduct["description"]:
        internalDescription = " ".join(internalProduct["description"])
        externalDescription = " ".join(externalProduct["description"])
        # print("Similarity = ", getTFIDFScoreForSentences(internalDescription, externalDescription))
        # print("Similarity = ", getTFIDFScoreForSentences(internalDescription, externalDescription))
        # print("Similarity = ", difflib.SequenceMatcher(None, internalDescription, externalDescription).ratio())
        # if "price" in externalHtmlData or "review" in externalHtmlData or '₹' in externalHtmlData:
        if getTFIDFScoreForSentences(internalDescription, externalDescription) > 0.01:
            if featureMatchingRequired:
                return productSimilarityScore(externalProduct, internalProduct)
    return False


def checkProductFeaturesRelevancy(externalProduct, internalProduct):
    internalFeatures = internalProduct["features"]["specifications"]
    externalFeatures = externalProduct["features"]["tableFeatures"]
    count = 0
    countForMatch = 0
    for key in internalFeatures:
        if key in externalFeatures.keys():
            count += 1
            internalVal = internalFeatures[key]
            externalVal = externalFeatures[key]
            if difflib.SequenceMatcher(None, internalVal, externalVal).ratio() > 0.5:
                countForMatch += 1
    if count == countForMatch:
        return False
    return True


def createVariationValues(val):
    return [str(val)] + [re.sub('[^A-Za-z0-9 ]+', ' ', str(val))] + [re.sub('[^A-Za-z0-9 ]+', '', str(val))]


def productSimilarityScore(externalProduct, internalProduct):
    internalFeatures = internalProduct["features"]
    externalTableFeatures = externalProduct["features"]["tableFeatures"]
    externalNonTableFeatures = externalProduct["features"]["nonTableFeatures"] + externalProduct["productNames"]
    externalFeatures = list(externalTableFeatures.values()) + externalNonTableFeatures
    externalFeatures = list(map(createVariationValues, externalFeatures))
    externalFeatures = " ".join([item for sublist in externalFeatures for item in sublist])

    count = 0
    featureVariationValues = 0
    for key in internalFeatures:
        featureVariationValues = createVariationValues(internalFeatures[key])
        # print("featureVariationValues=",featureVariationValues)
        for val in featureVariationValues:
            if val in externalFeatures:
                count += 1

    avgCount = count / float(len(featureVariationValues) * len(internalFeatures.keys()))
    # print(avgCount)
    if avgCount > 0.1:
        return True
    return False


def createGeneralDictForFeatures(featureList):
    features = dict()
    tempDict = dict()
    nonTabFeatures = []
    for featureItem in featureList:
        if type(featureItem) == tuple and len(featureItem) > 1:
            key = " ".join(re.findall("[a-z]+", str(featureItem[0])))
            tempDict[key] = str(featureItem[1])
        if type(featureItem) == tuple and len(featureItem) <= 1:
            nonTabFeatures.append(featureItem)
        if type(featureItem) == str:
            nonTabFeatures.append(featureItem)
    features["tableFeatures"] = tempDict
    features["nonTableFeatures"] = nonTabFeatures
    return features


def is_valid(url):
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def getImageUrls(url, modelName):
    response = requests.get(url)
    htmlData = response.content
    productPage = BeautifulSoup(htmlData, features="html.parser")
    htmlData = str(productPage)
    regexUrl = r"\b((?:https?://)?(?:(?:www\.)?(?:[\da-z\.-]+)\.(?:[a-z]{2,6})|(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])))(?::[0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])?(?:/[\w\.-]*)*/?)\b"
    allUrls = re.findall(regexUrl, htmlData)
    urls = []
    maximumImage = 0
    finalUrl = []
    for hyperlink in allUrls:
        if 'http' in hyperlink and (
                'jpeg' in hyperlink or 'jpg' in hyperlink or 'png' in hyperlink) and 'logo' not in hyperlink and 'footer' not in hyperlink and 'favicon' not in hyperlink:
            width, height = 0, 0
            try:
                conn = urllib.request.urlopen(hyperlink)
            except urllib.error.HTTPError as e:
                pass
            except urllib.error.URLError as e:
                pass
            else:
                urls.append(hyperlink)
                image = Image.open(conn)
                width, height = image.size
            if maximumImage < int(width) * int(height) and int(width) > 0 and int(height) > 0:
                maximumImage = int(width) * int(height)

    for link in urls:
        image = Image.open(urllib.request.urlopen(link))
        width, height = image.size
        if width * height >= maximumImage or modelName in hyperlink:
            finalUrl.append(link)
    return finalUrl


def get_all_images(url):
    soup = BeautifulSoup(requests.get(url).content, "html.parser")
    urls = []
    for img in soup.find_all("img"):
        img_url = img.attrs.get("src")
        if not img_url:
            # if img does not contain src attribute, just skip
            continue
        img_url = urljoin(url, img_url)
        try:
            pos = img_url.index("?")
            img_url = img_url[:pos]
        except ValueError:
            pass
            # finally, if the url is valid
        if is_valid(img_url):
            urls.append(img_url)
    return urls


def extractImagesFromUrl(url, pathname):
    pathname = "productImages/" + pathname
    if not os.path.isdir(pathname):
        os.makedirs(pathname)

    filename = os.path.join(pathname, url.split("/")[-1])
    image = Image.open(urllib.request.urlopen(url))
    image_file = requests.get(url)
    file = open(filename, "wb")
    file.write(image_file.content)
    file.close()
    return True


def getImageDimensionsUrls(urls):
    final_urls = []
    for url in urls:
        image = Image.open(urllib.request.urlopen(url))
        width, height = image.size
        print(url, width, height)
        if width > 50 and height > 50:
            final_urls.append(url)
    return final_urls
