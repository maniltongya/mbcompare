# README #

Requirements:
Python 3+ version

Steps to run:

```bat
1. pip install -r requirements.txt
2. venv\Scripts\activate
3. python -m src.MultiProcessing
4. python -m src.createExcelFile
```

Check the reports Directory for the Generated reports