from utils import utils

# removal of junk words from description data
removeWordsFromDescription = ['sign in', '&gt;', '&amp;', 'javascript seems to be disabled in your browser', 'for the best experience on our site, be sure to turn on javascript in your browser', '&times;', 'guest login', 'customer name', 'contact number', 'email', 'submit', 'contact us', 'newsletter', 'shopping policies', 'terms of use', 'return policy', 'privacy policy', 'cookies policy', 'disclaimer', 'it appears that your browser has javascript disabled.', 'this website requires your browser to be javascript enabled.', 'please enable javascript and reload this page.', 'it appears that your browser has cookies disabled.', 'the website requires your browser to enable cookies in order to login.', 'please enable cookies and reload this page.', 'internet explorer is not supported on this site. for the best experience please update your browser to edge or use google chrome', 'all', 'products', 'contact us', 'frequently asked questions', 'terms &amp; conditions', 'privacy policy', 'about us', 'order status', 'product batch certificate', 'product complaint reporting', 'menu', 'view cart', 'usd', 'inr', 'gbp', 'cad', 'aud', 'eur', 'jpy', 'pin on pinterest', 'home', 'about us', 'contact us', 'explore', 'plus', 'login', 'more', 'cart', 'share', '{{value}}', 'blog', 'deals', 'coupons', 'stop alerts', 'get alerts', 'all categories', '$category', 'mobiles', 'all mobiles', '{{availability}}', 'as on {{time}}', 'go to', '{{site}}', '&gt;', '{{similar_items.length}} similar items &gt;', '{{name}}', '₹{{effective_price}}', '₹{{price}}', 'go to {{site}} &gt;', '{{discount}}% off', 'best price of', '₹{{best_price}}', '{{.}}', '{{text}}']

# removing below words from links as most of them are not ecommerce links and it takes a lot of time to extract the information from these links
# as most of them are blogs or documents
stringNotToBePresentInLink = ['merckmillipore', 'grainger', 'fda', 'xml', 'livrosdeamor', 'yellow', 'pdf', 'docx', 'txt', 'mycolmed', 'medicinebank24', 'myntra', 'wikipedia', 'wms.co.uk', 'gimaitaly', 'technocarem', 'merckmillipore', 'grainger', 'fda', 'xml', 'livrosdeamor', 'yellow', 'pdf', 'docx' ,'txt', 'mycolmed']

numOfUrlsFromGoogle = 15

# inputFilePath = "config/InputFileWithFeaturesFull.xlsx"
inputFilePath = "inputFiles/InputFile.xlsx"

outputFilePath = "reports/ProductReport_"
